import { MRT_ColumnDef } from "material-react-table";
import { IPost } from "./post.interface";

export interface CreateModalProps {
  columns: MRT_ColumnDef<IPost>[];
  onClose: () => void;
  onSubmit: (values: IPost) => void;
  open: boolean;
}
