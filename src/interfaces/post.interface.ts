export interface IPost {
  id: number;
  title: string;
  text: string;
  image: string;
  url: string;
  active: 1 | null;
  sort_order: null;
  created_at: string | null;
  updated_at: string | null;
  deleted_at: string | null;
}
