import axios from "axios";

import { environment } from "./api.env";

const config = axios.create({
  baseURL: `${environment.hostIp}`,
});

export default config;
