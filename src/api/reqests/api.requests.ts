import { IPost } from "../../interfaces/post.interface";
import { main } from "../api.endpoints";
import API from "../axiosConfig";

export const getPosts = () => API.get<IPost[]>(main);

export const addPost = (post: IPost) => API.post(main, post);

export const updatePost = (post: IPost) => API.put(`${main}/${post.id}`, post);
export const deletePost = (post: IPost) => {
  return API.delete(`${main}/${post.id}`);
};
