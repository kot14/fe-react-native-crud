import { useMemo, useState, useEffect, useCallback } from "react";

import {
  MRT_Cell,
  MRT_Row,
  MaterialReactTable,
  MaterialReactTableProps,
} from "material-react-table";
import { Box, Tooltip, IconButton, Button } from "@mui/material";
import { type MRT_ColumnDef } from "material-react-table";
import { Edit, Delete } from "@mui/icons-material";

import { CreateNewAccountModal } from "./NewPostModal";
import {
  addPost,
  deletePost,
  getPosts,
  updatePost,
} from "../api/reqests/api.requests";
import { IPost } from "../interfaces/post.interface";

export const TableComponent = () => {
  useEffect(() => {
    getPosts().then((res) => {
      setPostList(res.data);
    });
  }, []);
  const [postList, setPostList] = useState<IPost[]>([]);
  const [createModalOpen, setCreateModalOpen] = useState(false);
  const [validationErrors, setValidationErrors] = useState<{
    [cellId: string]: string;
  }>({});
  const validateRequired = (value: string) => !!value.length;

  const handleDeleteRow = useCallback((row: MRT_Row<IPost>) => {
    if (!confirm(`Are you sure you want to delete ${row.getValue("title")}`)) {
      return;
    }
    deletePost(row.original).then((res) => {
      getPosts().then((res) => {
        setPostList(res.data);
      });
    });
  }, []);

  const handleCreateNewRow = (values: {
    image: string;
    title: string;
    text: string;
  }) => {
    const newPost: IPost = {
      ...values,
      id: 0,
      created_at: new Date().toJSON(),
      active: null,
      sort_order: null,
      url: values.image,
      updated_at: null,
      deleted_at: null,
    };
    addPost(newPost).then((res) => setPostList(postList.concat(res.data)));
  };

  const handleSaveRowEdits: MaterialReactTableProps<IPost>["onEditingRowSave"] =
    async ({ exitEditingMode, values }) => {
      if (!Object.keys(validationErrors).length) {
        updatePost(values).then((res) => {
          setPostList(
            postList.map((post) => (post.id !== res.data.id ? post : res.data))
          );
          exitEditingMode();
        });
      }
    };

  const handleCancelRowEdits = () => {
    setValidationErrors({});
  };

  const getCommonEditTextFieldProps = useCallback(
    (
      cell: MRT_Cell<IPost>
    ): MRT_ColumnDef<IPost>["muiTableBodyCellEditTextFieldProps"] => {
      return {
        error: !!validationErrors[cell.id],
        helperText: validationErrors[cell.id],
        onBlur: (event) => {
          const isValid =
            (cell.column.id === "id" && validateRequired(event.target.value)) ||
            (cell.column.id === "title" &&
              validateRequired(event.target.value)) ||
            (cell.column.id === "image" &&
              validateRequired(event.target.value)) ||
            (cell.column.id === "text" && validateRequired(event.target.value));
          if (!isValid) {
            setValidationErrors({
              ...validationErrors,
              [cell.id]: `${cell.column.columnDef.header} is required`,
            });
          } else {
            delete validationErrors[cell.id];
            setValidationErrors({
              ...validationErrors,
            });
          }
        },
      };
    },
    [validationErrors]
  );

  const columns = useMemo<MRT_ColumnDef<IPost>[]>(
    () => [
      {
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
        enableEditing: false,
        accessorKey: "id",
        header: "Id",
        size: 20,
      },
      {
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
        accessorKey: "image",
        header: "Image",
        size: 50,
        Cell: ({ cell }) => (
          <img width="50px" height="50px" src={cell.getValue<string>()} />
        ),
      },
      {
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
        accessorKey: "title",
        header: "Title",
        size: 100,
      },
      {
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
        accessorKey: "text",
        header: "Text",
        size: 20,
      },
    ],
    [getCommonEditTextFieldProps]
  );

  return (
    <>
      <MaterialReactTable
        displayColumnDefOptions={{
          "mrt-row-actions": {
            muiTableHeadCellProps: {
              align: "center",
            },
            size: 20,
          },
        }}
        initialState={{
          isFullScreen: true,
          density: "compact",
          sorting: [{ id: "id", desc: true }],
          pagination: { pageSize: 20, pageIndex: 0 },
        }}
        enableFullScreenToggle={false}
        positionActionsColumn="last"
        onEditingRowSave={handleSaveRowEdits}
        onEditingRowCancel={handleCancelRowEdits}
        enableEditing={true}
        columns={columns}
        data={postList}
        renderRowActions={({ row, table }) => (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <Tooltip arrow placement="left" title="Edit">
              <IconButton onClick={() => table.setEditingRow(row)}>
                <Edit />
              </IconButton>
            </Tooltip>
            <Tooltip arrow placement="right" title="Delete">
              <IconButton color="error" onClick={() => handleDeleteRow(row)}>
                <Delete />
              </IconButton>
            </Tooltip>
          </Box>
        )}
        renderTopToolbarCustomActions={() => (
          <Button
            color="primary"
            onClick={() => setCreateModalOpen(true)}
            variant="contained"
          >
            Create New Post
          </Button>
        )}
      />
      <CreateNewAccountModal
        columns={columns.filter((column) => column.accessorKey !== "id")}
        open={createModalOpen}
        onClose={() => setCreateModalOpen(false)}
        onSubmit={handleCreateNewRow}
      />
    </>
  );
};
