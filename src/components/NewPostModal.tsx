import { useState } from "react";

import {
  Dialog,
  DialogTitle,
  DialogContent,
  Stack,
  TextField,
  DialogActions,
  Button,
} from "@mui/material";
import { CreateModalProps } from "../interfaces/table.interface";

export const CreateNewAccountModal = ({
  open,
  columns,
  onClose,
  onSubmit,
}: CreateModalProps) => {
  const clearFields = () =>
    columns.reduce((acc, column) => {
      acc[column.accessorKey ?? ""] = "";
      return acc;
    }, {} as any);

  const [values, setValues] = useState<any>(clearFields());
  const [error, setError] = useState<string[]>([]);

  const handleSubmit = () => {
    const fields = Object.keys(values).filter(
      (field) => values[field].length === 0
    );
    console.log(values);
    if (fields.length === 0) {
      onSubmit(values);
      setValues(clearFields());
      onClose();
    } else {
      setError(fields);
    }
  };

  return (
    <Dialog open={open}>
      <DialogTitle textAlign="center">Create New Post</DialogTitle>
      <DialogContent>
        <form onSubmit={(e) => e.preventDefault()}>
          <Stack
            sx={{
              width: "100%",
              minWidth: { xs: "300px", sm: "360px", md: "400px" },
              gap: "1.5rem",
            }}
          >
            {columns.map((column) => (
              <TextField
                key={column.accessorKey}
                label={column.header}
                name={column.accessorKey}
                error={error.includes(column.accessorKey as string)}
                helperText={
                  error.includes(column.accessorKey as string)
                    ? "is required"
                    : ""
                }
                onChange={(e) => {
                  setValues({ ...values, [e.target.name]: e.target.value });
                }}
              />
            ))}
          </Stack>
        </form>
      </DialogContent>
      <DialogActions sx={{ p: "1.25rem" }}>
        <Button onClick={onClose}>Cancel</Button>
        <Button color="primary" onClick={handleSubmit} variant="contained">
          Create New Post
        </Button>
      </DialogActions>
    </Dialog>
  );
};
